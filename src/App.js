import React, { useRef } from 'react';
import logo from './logo.svg';
import './App.css';
import Iframe from 'react-iframe'

const botIFrame=React.forwardRef((props, ref) => (
  <button ref={ref} className="FancyButton">
    {props.children}
  </button>
))

function App() {
  
  const iframeEl = useRef(null);
  const hellobutton_click = () => { 
    iframeEl.current.contentWindow.postMessage({name:'hello', text:'hello'},'*')
  }
  return (
    <div className="App">
      <h1>
        <button onClick={hellobutton_click}>Hello</button>
        <button>Something else</button>
      </h1>
      <iframe src="http://localhost:3000" height="300" width="300" ref={iframeEl}></iframe>
     
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
