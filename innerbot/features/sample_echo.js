/**
 * Copyright (c) Microsoft Corporation. All rights reserved.
 * Licensed under the MIT License.
 */

const EchoWelcomeCondition = async (message) => {
    return (message.external_command === 'welcome')
  }
  
module.exports = function(controller) {
    controller.hears(EchoWelcomeCondition, 'message', async (bot, message) => {
        await bot.reply(message, "Hello External Stranger!")
    
      })
    controller.hears('sample','message', async(bot, message) => {
        await bot.reply(message, 'I heard a sample message.');
    });

    controller.on('message', async(bot, message) => {
        await bot.reply(message, `Echo: ${ message.text }`);
    });

}